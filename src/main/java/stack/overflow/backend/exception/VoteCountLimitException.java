package stack.overflow.backend.exception;

public class VoteCountLimitException extends RuntimeException {

    public VoteCountLimitException(String message) {
        super(message);
    }

    public VoteCountLimitException(String message, Throwable cause) {
        super(message, cause);
    }
}
