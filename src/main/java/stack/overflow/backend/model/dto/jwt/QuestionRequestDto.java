package stack.overflow.backend.model.dto.jwt;

import lombok.Getter;
import lombok.Setter;
import stack.overflow.backend.model.entity.Tag;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Getter
@Setter
public class QuestionRequestDto {

    @NotBlank String title;

    @NotBlank String description;

    Set<Tag> tags;
}
