package stack.overflow.backend.model.dto.vote;

import stack.overflow.backend.model.enumeration.Vote;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public record QuestionVoteRequestDto(
        @NotNull Vote vote,
        @NotNull @Positive Long questionId) {
}
