package stack.overflow.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stack.overflow.backend.model.entity.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
