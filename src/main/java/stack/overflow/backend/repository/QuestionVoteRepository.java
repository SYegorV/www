package stack.overflow.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import stack.overflow.backend.model.entity.Account;
import stack.overflow.backend.model.entity.Question;
import stack.overflow.backend.model.entity.QuestionVote;
import stack.overflow.backend.model.enumeration.Vote;

import java.util.Optional;

public interface QuestionVoteRepository extends JpaRepository<QuestionVote, Long> {
    boolean existsByOwnerAndQuestion(Account owner, Question question);
    Optional<QuestionVote> findByQuestionAndOwner(Question question, Account owner);
}
