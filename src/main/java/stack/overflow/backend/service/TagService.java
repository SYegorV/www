package stack.overflow.backend.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import stack.overflow.backend.model.entity.Tag;

public interface TagService {
    Tag save(Tag tag);
    Page<Tag> findAll(Pageable pageable);
    void delete(Long id);
}
