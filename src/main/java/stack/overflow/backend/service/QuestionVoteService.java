package stack.overflow.backend.service;

import stack.overflow.backend.model.entity.Account;
import stack.overflow.backend.model.entity.QuestionVote;

public interface QuestionVoteService {
    QuestionVote save(QuestionVote vote);
    void delete(Long id, Account account);
}
