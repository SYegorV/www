package stack.overflow.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stack.overflow.backend.exception.EntityNotFoundException;
import stack.overflow.backend.model.entity.Question;
import stack.overflow.backend.repository.QuestionRepository;
import stack.overflow.backend.service.QuestionService;


@RequiredArgsConstructor
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    @Override
    public Question findById(Long id) {
        if (questionRepository.findById(id).isPresent()) {
            return questionRepository.findById(id).get();
        }
        throw new EntityNotFoundException("Question does not exist");
    }

    @Transactional
    @Override
    public Question save(Question question) {
        return questionRepository.save(question);
    }
}
