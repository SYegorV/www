package stack.overflow.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stack.overflow.backend.exception.AccessIsDeniedException;
import stack.overflow.backend.exception.EntityNotFoundException;
import stack.overflow.backend.exception.VoteCountLimitException;
import stack.overflow.backend.model.entity.Account;
import stack.overflow.backend.model.entity.QuestionVote;
import stack.overflow.backend.repository.QuestionVoteRepository;
import stack.overflow.backend.service.QuestionVoteService;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class QuestionVoteServiceImpl implements QuestionVoteService {

    private final QuestionVoteRepository questionVoteRepository;

    @Transactional
    @Override
    public QuestionVote save(QuestionVote vote) {
        if (questionVoteRepository.existsByOwnerAndQuestion(vote.getOwner(), vote.getQuestion())) {
            throw new VoteCountLimitException("This user has already voted for this question");
        }
        return questionVoteRepository.save(vote);
    }

    @Override
    public void delete(Long id, Account account) {
        Optional<QuestionVote> optionalQV = questionVoteRepository.findById(id);
        if (optionalQV.isEmpty()){
            throw new EntityNotFoundException("Question Vote not found");
        }
        QuestionVote questionVote = optionalQV.get();
        if (!questionVote.getOwner().equals(account)){
            throw new AccessIsDeniedException("The vote for the question does not belong to the account that initiated the request");
        }
        questionVoteRepository.delete(questionVote);
    }

}
