package stack.overflow.backend.service;

import stack.overflow.backend.model.entity.Question;

public interface QuestionService {

    public Question findById(Long id);

    Question save(Question question);
}
