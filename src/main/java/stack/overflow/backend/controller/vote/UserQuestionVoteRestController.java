package stack.overflow.backend.controller.vote;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stack.overflow.backend.model.dto.vote.QuestionVoteRequestDto;
import stack.overflow.backend.model.entity.Account;
import stack.overflow.backend.model.entity.QuestionVote;
import stack.overflow.backend.service.QuestionService;
import stack.overflow.backend.service.QuestionVoteService;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/api/v1/user/question-votes")
public class UserQuestionVoteRestController {

    private final QuestionVoteService questionVoteService;
    private final QuestionService questionService;


    @PostMapping
    public ResponseEntity<Void> addQuestionVote(@RequestBody @Valid QuestionVoteRequestDto questionVoteRequestDto) {
        Account currentUser = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        QuestionVote questionVote = new QuestionVote();
        questionVote.setVote(questionVoteRequestDto.vote());
        questionVote.setQuestion(questionService.findById(questionVoteRequestDto.questionId()));
        questionVote.setOwner(currentUser);
        questionVote.setCreationDate(LocalDateTime.now());

        questionVoteService.save(questionVote);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{questionVoteId}")
    public ResponseEntity<Void> deleteQuestionVote(@PathVariable Long questionVoteId) {
        Account currentUser = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        questionVoteService.delete(questionVoteId, currentUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
