package stack.overflow.backend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stack.overflow.backend.model.entity.Tag;
import stack.overflow.backend.service.TagService;


@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/api/v1/moderator/tags")
public class ModeratorTagRestController {

    private final TagService tagService;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Page<Tag>> getTags (Pageable pageable) {
        Page<Tag> tags = tagService.findAll(pageable);
       return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(tags);
    }

    @DeleteMapping("/{tagId}")
    public ResponseEntity<Void> delete(@PathVariable("tagId") Long tagId) {
        tagService.delete(tagId);
        return ResponseEntity.ok().build();
    }
}
