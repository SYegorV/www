package stack.overflow.backend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stack.overflow.backend.model.dto.jwt.QuestionRequestDto;
import stack.overflow.backend.model.entity.Account;
import stack.overflow.backend.model.entity.Question;
import stack.overflow.backend.service.QuestionService;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping
public class UserQuestionRestController {

    private final QuestionService questionService;

    @PostMapping("/api/v1/user/questions")
    public ResponseEntity<Void> create(@RequestBody @Valid QuestionRequestDto questionRequestDto) {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Question question = new Question();
        question.setTitle(questionRequestDto.getTitle());
        question.setDescription(questionRequestDto.getDescription());
        question.setTags(questionRequestDto.getTags());
        question.setCreationDate(LocalDateTime.now());
        question.setModificationDate(LocalDateTime.now());
        question.setOwner(account);

        questionService.save(question);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}