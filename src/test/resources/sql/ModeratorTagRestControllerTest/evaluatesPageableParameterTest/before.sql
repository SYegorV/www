TRUNCATE TABLE accounts CASCADE;
TRUNCATE TABLE permissions CASCADE;
TRUNCATE TABLE tags CASCADE;

INSERT INTO permissions(id, name)
VALUES (1, 'ADMIN'),
       (2, 'MODERATOR'),
       (3, 'USER');

INSERT INTO accounts(id, username, password, creation_date, enabled)
VALUES (1, 'user1', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', current_timestamp, true);

INSERT INTO accounts_permissions(account_id, permission_id)
VALUES (1, 2);

INSERT INTO tags(id, name)
VALUES (1, 'tag1'),
       (2, 'tag2'),
       (3, 'tag3'),
       (4, 'tag4'),
       (5, 'tag5'),
       (6, 'tag6'),
       (7, 'tag7'),
       (8, 'tag8'),
       (9, 'tag9'),
       (10, 'tag10'),
       (11, 'tag11'),
       (12, 'tag12'),
       (13, 'tag13'),
       (14, 'tag14'),
       (15, 'tag15'),
       (16, 'tag16'),
       (17, 'tag17'),
       (18, 'tag18'),
       (19, 'tag19'),
       (20, 'tag20'),
       (21, 'tag21'),
       (22, 'tag22'),
       (23, 'tag23'),
       (24, 'tag24'),
       (25, 'tag25'),
       (26, 'tag26');
