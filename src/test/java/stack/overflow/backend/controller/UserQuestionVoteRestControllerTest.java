package stack.overflow.backend.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import stack.overflow.backend.IntegrationTestContext;
import stack.overflow.backend.model.dto.vote.QuestionVoteRequestDto;
import stack.overflow.backend.model.enumeration.Vote;
import stack.overflow.backend.repository.AccountRepository;
import stack.overflow.backend.repository.QuestionRepository;
import stack.overflow.backend.repository.QuestionVoteRepository;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserQuestionVoteRestControllerTest extends IntegrationTestContext {

    @Autowired
    private QuestionVoteRepository questionVoteRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVote/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVote/after.sql")
    @Test
    public void addQuestionVote() throws Exception {
        QuestionVoteRequestDto dto = new QuestionVoteRequestDto(Vote.USEFUL, 1L);
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(post("/api/v1/user/question-votes")
                        .header("Authorization", "Bearer " + token)
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        if (questionVoteRepository.findByQuestionAndOwner(
                questionRepository.findById(1L).get(),
                accountRepository.findById(1L).get()).isEmpty()) {
            Assert.fail("Голос не добавился в БД");
        }
    }

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVoteVoteCountLimitException/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVoteVoteCountLimitException/after.sql")
    @Test
    public void addQuestionVoteVoteCountLimitException() throws Exception {
        QuestionVoteRequestDto dto = new QuestionVoteRequestDto(Vote.USEFUL, 1L);
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(post("/api/v1/user/question-votes")
                .header("Authorization", "Bearer " + token)
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON));
        mockMvc.perform(post("/api/v1/user/question-votes")
                        .header("Authorization", "Bearer " + token)
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVoteEntityNotFoundException/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/UserQuestionVoteRestControllerTest/addQuestionVoteEntityNotFoundException/after.sql")
    @Test
    public void addQuestionVoteEntityNotFoundException() throws Exception {
        QuestionVoteRequestDto dto = new QuestionVoteRequestDto(Vote.USEFUL, 2L);
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(post("/api/v1/user/question-votes")
                        .header("Authorization", "Bearer " + token)
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
