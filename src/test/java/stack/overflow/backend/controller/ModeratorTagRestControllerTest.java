package stack.overflow.backend.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import stack.overflow.backend.IntegrationTestContext;
import stack.overflow.backend.repository.TagRepository;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ModeratorTagRestControllerTest extends IntegrationTestContext {

    @Autowired
    TagRepository tagRepository;

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/getTagsTest/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/getTagsTest/after.sql")
    @Test
    public void getTagsTest() throws Exception {
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(get("/api/v1/moderator/tags")
                    .header("authorization", "Bearer " + token)
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(1))
                .andExpect(jsonPath("$.content[0].name").value("tag1"));
    }

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/evaluatesPageableParameterTest/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/evaluatesPageableParameterTest/after.sql")
    @Test
    public void evaluatesPageableParameter() throws Exception {
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(get("/api/v1/moderator/tags")
                .header("authorization", "Bearer " + token)
                .param("page", "2")
                .param("size", "10")
                .param("sort", "id,desc")
                .param("sort", "name,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.pageNumber").value(2))
                .andExpect(jsonPath("$.pageable.pageSize").value(10))
                .andExpect(jsonPath("$.sort.sorted").value(true));
    }

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/deleteTagTest/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/ModeratorTagRestControllerTest/deleteTagTest/after.sql")
    @Test
    public void deleteTagsTest() throws Exception {
        Long id = 1L;
        if (tagRepository.findById(id).isEmpty()) {
            Assert.fail("Тэга с таким Id нет");
        }
        String token = testUtil.getToken("user1", "password");
        mockMvc.perform(delete("/api/v1/moderator/tags/{tagId}", id)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
        if (tagRepository.findById(id).isPresent()) {
            Assert.fail("Тэг не удалён");
        }
    }
}
