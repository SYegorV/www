package stack.overflow.backend.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import stack.overflow.backend.IntegrationTestContext;
import stack.overflow.backend.model.dto.jwt.QuestionRequestDto;
import stack.overflow.backend.model.entity.Tag;
import stack.overflow.backend.repository.QuestionRepository;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserQuestionRestControllerTest extends IntegrationTestContext {

    @Autowired
    private QuestionRepository questionRepository;

    @Sql(executionPhase = BEFORE_TEST_METHOD, value = "/sql/UserQuestionRestControllerTest/addQuestionTest/before.sql")
    @Sql(executionPhase = AFTER_TEST_METHOD, value = "/sql/UserQuestionRestControllerTest/addQuestionTest/after.sql")
    @Test

    public void addQuestionTest() throws Exception {

        QuestionRequestDto questionRequestDto = new QuestionRequestDto();

        Set<Tag> tags = new HashSet<>();
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("tag1");
        tags.add(tag);

        questionRequestDto.setTitle("test title");
        questionRequestDto.setDescription("test description");
        questionRequestDto.setTags(tags);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<QuestionRequestDto>> violations = validator.validate(questionRequestDto);
        assertEquals(0, violations.size());

        String token = testUtil.getToken("user1", "password");

        mockMvc.perform(post("/api/v1/user/questions")
                        .header("Authorization", "Bearer " + token)
                        .content(objectMapper.writeValueAsString(questionRequestDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        if (questionRepository.findById(1L).isEmpty()) {
            Assert.fail("Вопрос не добавился в БД");
        }
    }
}

